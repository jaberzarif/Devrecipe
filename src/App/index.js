import React, { Component } from 'react';
import logo from '../Assets/img/logo.png';
import Header from '../Components/Header';
import About from '../Components/About';
import Technology from '../Components/Technology';
import Services from '../Components/Services';
import Customers from '../Components/Customers';
import Team from '../Components/Team';
import Footer from '../Components/Footer';
import '../Components/Header/style.css';
import '../Components/About/style.css';
import '../Components/Technology/style.css';
import '../Components/Services/style.css';
import '../Components/Customers/style.css';
import '../Components/Team/style.css';
import '../Components/Footer/style.css';
import '../Assets/css/animate.css';


class App extends Component {
  render() {
    return <div className="App">
      <Header/>
      <About/>
      <Technology/>
      <Services/>
      <Customers/>
      <Team/>
      <Footer/>
    </div>
  }
}

export default App;
