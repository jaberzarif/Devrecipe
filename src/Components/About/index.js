import React, { Component } from 'react';
import Soustraction from '../../Assets/img/Soustraction1.png';
import '../About/style.css';
import '../../Assets/css/animate.css';


class About extends Component {
  render() {
    return <div id="About" className="About-us rellax animated fadeIn" data-rellax-speed="-5">
           <div className="Soustraction">
           <div className="container middle">
              <h1 className="title pseudo_border animated zoomIn">About us</h1>
                <div className="text-div">
                <h2 className="display-4 animated zoomIn">We are a software company, and you must have an idea, you are here to build it! </h2>
              </div>
            </div>
          </div>
        </div>
    ;
  }
}

export default About;
