import React, { Component } from 'react';
import Karizma from '../../Assets/img/NoPath2.png';
import Five from '../../Assets/img/Image4.png';
import Ifes from '../../Assets/img/Image5.png';
import Click from '../../Assets/img/NoPath.png';
import smsm from '../../Assets/img/smsm.png';
import tunes from '../../Assets/img/tunes.png';
import microtechx from '../../Assets/img/microtechx.png';
import enfantsJasmin from '../../Assets/img/enfants-jasmin.png'
import '../Customers/style.css';
import '../../Assets/css/animate.css';

class Customers extends Component {
  render() {
    return <div id="Customers" className="Customers rellax" data-rellax-speed="-5">
    
           <div className="container middle">
              <h1 className="title pseudo_border animated zoomIn">Happy customers</h1>
              <div className="logos-customers animated zoomIn">
                <img className="log " src={Karizma} alt="Karizma logo"/>
                <img className="log" src={Five} alt="Five points logo"/>
                <img className="log" src={Ifes} alt="IFES logo"/>
                <img className="log" src={Click} alt="3Click logo"/>
              <div class="space">
                <img className="log" src={smsm} alt="smsm logo"/>
                <img className="log" src={tunes} alt="tunes logo"/>
                <img className="log" src={microtechx} alt="microtechx logo"/>
                <img className="log" src={enfantsJasmin} alt="enfants Jasmin logo"/>
              </div>
                <p className="text">And a bunch from Europe & Africa</p>
              </div>
              
            </div>
          </div>;
          
          
  }
}

export default Customers;
