import React, { Component } from 'react';
import '../Footer/style.css';
import '../../Assets/css/animate.css';

class Footer extends Component {
  render() {
    return <div id="Contact" className="Footer background">
    
           <div className="count middle">
            <section id="contact">
            <div>
                <div className="row">
                    <div className="col-lg-6">
                        <div className="jumbotron spacing">
                            <h1 className="large animated fadeInUp">Let's talk</h1>
                            <h5 className="animated fadeInUp">
                                We'd love to hear what you're working on and how we might be able to help.
                            </h5>

                            <ul className="list-group">
                                <li className="list-group-item animated fadeInUp">
                                <i class="fal fa-building"></i>
                                    Cyberparc, Route El Hemma, Tozeur, 2210, Tunisia.
                                </li>

                                <li className="list-group-item">
                                    <i className="fal fa-envelope"></i>
                                    hello@devrecipe.com
                                </li>

                                <li className="list-group-item">
                                    <i className="fal fa-phone fa-flip-horizontal"></i>
                                    +216 28 657 882
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="jumbotron spacing animated fadeInUp">
                            <form id="contact-form" className="contacts" novalidate>
                                <input id="contact-name" type="text" className="form-control" placeholder="Your Name" required/>
                                <input id="contact-email" type="email" className="form-control" placeholder="Your Email Address" required/>
                                <textarea id="contact-message" rows="5" className="form-control" placeholder="How can we help you?" required></textarea>

                                <small className="float-left">All fields are required</small>
                                <div className="text-right">
                                    <button id="contact-send" type="submit" className="btn btn-white-tech">Send</button>
                                </div>
                            </form>

                            
                        </div>
                    </div>
                </div>
            </div>
           </section>
           <footer>
            <div className="footer_content animated fadeInUp">
                <div className="container text-center">
                    <ul className="nav justify-content-center">
                        <li className="nav-item">
                            <a className="nav-icon" href="https://www.facebook.com/devrecipe" target="_blank">
                                <i  className="fab fa-facebook"></i>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-icon" href="https://www.instagram.com/devrecipe" target="_blank">
                                <i  className="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-icon" href="https://www.twitter.com/devrecipe" target="_blank">
                                <i  className="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-icon" href="https://www.linkedin.com/in/devrecipe" target="_blank">
                                <i  className="fab fa-linkedin"></i>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-icon" href="https://www.github.com/devrecipe" target="_blank">
                                <i className="fab fa-github"></i>
                            </a>
                        </li>
                    </ul>

                    <p>Crafted with
                        <i className="fa fa-heart"></i> &
                        <i className="fa fa-coffee"></i> in Tozeur.</p>

                    <p>Have a nice day </p>
                </div>
            </div>
        </footer>
        </div>
        </div>;
          
          
  }
}

export default Footer;