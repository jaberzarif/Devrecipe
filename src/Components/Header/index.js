import React, { Component } from 'react';
import LogoGreen from '../../Assets/img/logo.png';
import LogoWhite from '../../Assets/img/logo_full_white.png';
import background from '../../Assets/img/Groupe328.png'; 
import greenWave from '../../Assets/img/Trace3.png';
import devices from '../../Assets/img/Groupe319.png';
import './style.css';
import '../../Assets/css/animate.css';


var $ = window.$

class Header extends Component {
 componentDidMount(){
    $(document).ready(function(){
      $(window).scroll(function(){
        var scroll = $(window).scrollTop();

        if (scroll > 0) {
          $(".navbar").addClass('scrolled');
          $('.navbar img').attr('src', LogoWhite);
        } else {
          $(".navbar").removeClass('scrolled')
          $('.navbar img').attr('src', LogoGreen);
        }
      })
    })
  }
  
  render() {
    return <header className="App-header">
      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container">
        <img className="mobile-logo" src={LogoWhite} />
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">            <span className="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarNav">
            <img src={LogoGreen} className="App-logo" alt="Devrecipe Logo" />
            <ul className="navbar-nav mr-auto" />
            <ul className="navbar-nav">
              <li className="nav-item active">
                <a className="nav-link" href="#About">About</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#Technology">Technology</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#Services">Services</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#Customers">Customers</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#Team">Team</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#Contact">Contact</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="container valign-container animated fadeIn">
        <div className="left-div valign">
          <h1 className="display-3">Great recipes make tasty apps</h1>
          <button className="btn btn-white">request a quote</button>
        </div>
      </div>

      <div className="green-wave">
        <img className="devices animated zoomIn" src={devices} alt="Devices"/>
      </div>
    </header>;
  }
}

export default Header;
