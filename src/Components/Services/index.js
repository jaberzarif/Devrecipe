import React, { Component } from 'react';
import recipes from '../../Assets/img/Groupe7.png'
import '../Services/style.css';
import '../../Assets/css/animate.css';

class Services extends Component {
  render() {
    return <div id="Services" className="Services rellax" data-rellax-speed="-5">
    
           <div className="container middle">
              <h1 className="title pseudo_border">Services</h1>
              <div className="fonts">
              <div>
              <ul className="list-group">
                                <li className="list-group-item it">
                                <span className="green-circle">
                                <i className="fal fa-globe"></i>
                                </span>
                                     <span className="text-service">Web Developement</span>
                                </li>

                                <li className="list-group-item it">
                                <span className="green-circle">
                                <i className="fal fa-mobile icon"></i>
                                </span>
                                     <span className="text-service">Mobile Developement</span>
                                </li>

                                <li className="list-group-item it">
                                <span className="green-circle">
                                <i className="fal fa-cloud"></i>
                                </span >
                                    <span className="text-service">DevOps & Cloud Computing</span>
                                </li>
                                <li className="list-group-item it">
                                <span className="green-circle">
                                <i className="fal fa-graduation-cap"></i>
                                </span>
                                     <span className="text-service">Coaching & Consulty</span>
                                </li>
                            </ul>
            </div>
            </div>
            </div>
            <div className="Soustraction-5"></div>
            <div className="recipes">
            <img src={recipes} alt="recipes"/>
            </div>
          </div>;
          
          
  }
}

export default Services;
