import React, { Component } from 'react';
import Mabo from '../../Assets/img/Image1.png';
import Jaber from '../../Assets/img/Image2.png';
import Dorsaf from '../../Assets/img/Image3.png';
import User from '../../Assets/img/Image-user.png';
import '../Team/style.css';
import '../../Assets/css/animate.css';

class Team extends Component {
  render() {
    return <div id="Team" className="Team rellax" data-rellax-speed="-5">
    
           <div className="container middle">
              <h1 className="title pseudo_border">The Team</h1>
              <div className="team-pictures">
              <span className="col-sm-4">  
                <img className="img" src={Mabo} alt=""/>
                <span className="texts"><strong className="Col"  >Mohamed Anas</strong> <br/>CEO & Kitchen Head</span>
            
              </span>
              <span className="col-sm-4">
                <img className="img" src={Jaber} alt=""/>
                <span className="texts"><strong className="Col">Jaber Zarif</strong> <br/> Frontend Chef</span>
             
              </span>
              <span className="col-sm-4">
                <img className="img" src={Dorsaf} alt=""/>
                <span className="texts"><strong className="Col">Dorsaf Hamad</strong> <br/> User Experience Artist</span>
              </span>
              </div>
              
              <div className=" team-pictures space-top">
              
              <span className="col-sm-4 sm">
                <img className="img" src={User} alt=""/>
                <span className="texts"><strong className="Col">Amal Touhami</strong> <br/>Backend Chef</span>
              </span>
              
              <span className="col-sm-4 sm">
                <img className="img" src={User} alt=""/>
                <span className="texts"><strong className="Col">Afef Mabrouk</strong> <br/>Web Design Artist</span>
              </span>
              
              </div>
              
              <h1 className="title-adventure">seeking adventures?</h1>
              <button className="btn btn-white-tech">apply for internship</button>
             
            </div>
          </div>;
          
          
  }
}

export default Team;