import React, { Component } from 'react';
import Node from '../../Assets/img/Nodejs.png';
import Ricon from '../../Assets/img/React-icon.png';
import Electron from '../../Assets/img/electron.png';
import Kubernetes from '../../Assets/img/Kubernetes.png';
import Microsoft from '../../Assets/img/microsoft-azure-3.png';
import Mongodb from '../../Assets/img/mongodb.png';
import '../Technology/style.css';
import '../../Assets/css/animate.css';
import '../../Assets/js/tabs.js';


class Technology extends Component {
  constructor() {
    super()

    this.state = {
      currentTab: 'node'
    }

    this.TABS = {
      'node': 'Node is ...',
      'react': 'React is ...',
      'electron': 'Electron is ...',
      'kubernetes': 'Kubernetes is ...',
      'azure': 'Azure is ...',
      'mongodb': 'MongoDB is ...'
    }
  }

  toggleTab(name) {
    this.setState({
      currentTab: name
    })
  }

  render() {  
    return <div id="Technology" className="technology rellax" data-rellax-speed="-5">
        <div className="container middle">
          <h1 className="title pseudo_border">Technology</h1>
            <div className="images-div">
              <button className="bts" onClick={() => { this.toggleTab('node') }}><img className="img-tech" src={Node} alt="Node-icon"/></button>
              <button className="bts" onClick={() => { this.toggleTab('react') }}><img className="img-tech" src={Ricon} alt="React-icon"/></button>
              <button className="bts" onClick={() => { this.toggleTab('electron') }}><img className="img-tech" src={Electron} alt="Electron-icon"/></button>
              <button className="bts" onClick={() => { this.toggleTab('kubernetes') }}><img className="img-tech" src={Kubernetes} alt="Kubernetes-icon"/></button>
              <button className="bts" onClick={() => { this.toggleTab('azure') }}><img className="img-tech" src={Microsoft} alt="Microsoft-azure-icon"/></button>
              <button className="bts" onClick={() => { this.toggleTab('mongodb') }}><img className="img-tech" src={Mongodb} alt="Mongodb-icon"/></button>
            </div>

            <div className="text-div">
                <p className="tabcontent">{ this.TABS[this.state.currentTab] }</p>
                <button className="btn btn-white-tech space">Learn more</button>
            </div>
        </div>

        <div className="Soustraction-2"></div>
        <div className="Soustraction-3"></div>
        <div className="Soustraction-4"></div>
      </div>;
  }
}

export default Technology;
